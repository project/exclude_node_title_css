<?php

/**
 * Implements hook_form_alter().
 *
 * If user chose to hide the title using CSS, display checkbox only if the default theme supports hiding the title with CSS.
 */
function exclude_node_title_css_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['type']) && isset($form['#node']) && $form['type']['#value'] . '_node_form' == $form_id) {
    if (variable_get('exclude_node_title_hide_using_css', 0)) {
      $theme_default = variable_get('theme_default', 'bartik');
      $supported_themes = variable_get('exclude_node_title_themes_supporting_css', array());
      if (isset($form['exclude_node_title']) && !in_array($theme_default, $supported_themes)) {
        unset($form['exclude_node_title']);
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add option to hide title using CSS.
 */
function exclude_node_title_css_form_exclude_node_title_admin_settings_alter(&$form, &$form_state, $form_id) {
  $form['exclude_node_title_hide_using_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide node title using CSS'),
    '#description' => t('Select if you wish to use CSS to hide the node title. This will make your page accessible for both screen-readers and search engines as they utilize heading tags. You will need to place this variable "$exclude_node_title_class" in the page title class attribute, in both your page.tpl.php and node.tpl.php template for this feature to take effect.'),
    '#default_value' => variable_get('exclude_node_title_hide_using_css', 0),
    '#weight' => -2,
  );
  $form['exclude_node_title_themes_supporting_css'] = array(
    '#type' => 'select',
    '#options' => drupal_map_assoc(array_keys(list_themes())),
    '#description' => t('Select the themes in which the page.tpl.php and node.tpl.php templates contain the variable "$exclude_node_title_class". You can find an example <a href="@link">here</a>.', array('@link' => 'https://www.drupal.org/node/1057044#comment-4080172')),
    '#multiple' => TRUE,
    '#default_value' => variable_get('exclude_node_title_themes_supporting_css', array()),
    '#states' => array(
      'visible' => array(
        'input[name="exclude_node_title_hide_using_css"]' => array('checked' => TRUE),
      ),
    ),
    '#weight' => -1,
  );
}

/**
 * Implements hook_preprocess_page().
 *
 * Override exclude_node_title_preprocess_page() so it uses _exclude_node_title_css_preprocess().
 */
function exclude_node_title_css_preprocess_page(&$vars) {
  if (!user_access('use exclude node title') || (arg(0) == 'node' && arg(1) == 'add')) {
    return;
  }

  if (arg(0) == 'node' && is_numeric(arg(1))) {
    switch (arg(2)) {
      case 'edit':
        $view_mode = 'nodeform';
        break;
      case 'delete':
        return; // delete pages show you all information in title, we should not remove it
      default:
        $view_mode = 'full';
        break;
    }

    _exclude_node_title_css_preprocess($vars, arg(1), $view_mode);
  } else if (isset($vars['page']['content']['system_main']['#node_edit_form']) && $vars['page']['content']['system_main']['#node_edit_form'] == TRUE) {
    _exclude_node_title_css_preprocess($vars, $vars['page']['content']['system_main']['#node'], 'nodeform');
  }
}

/**
 * Implements hook_preprocess_node().
 *
 * Override exclude_node_title_preprocess_node() so it uses _exclude_node_title_css_preprocess().
 */
function exclude_node_title_css_preprocess_node(&$vars) {
  if (user_access('use exclude node title')) {
    _exclude_node_title_css_preprocess($vars, $vars['node'], $vars['view_mode']);
  }
}

/**
 * Remove the title from the variables array.
 *
 * Override _exclude_node_title_preprocess() so we support hiding the title with CSS.
 *
 * @see https://www.drupal.org/node/1057044#comment-4080172
 */
function _exclude_node_title_css_preprocess(&$vars, $node, $view_mode) {
  if (_exclude_node_title($node, $view_mode)) {
    $hide_using_css = variable_get('exclude_node_title_hide_using_css', 0);
    if ($hide_using_css) {
      $vars['exclude_node_title_class'] = drupal_html_class('element-hidden');
      $vars['title'] = $node->title;
    }
    else {
      $vars['title'] = '';
    }
    if ($view_mode == 'nodeform') {
      // Also remove the field from the form
      $vars['page']['content']['system_main']['title']['#access'] = false;
    }
  }
}
